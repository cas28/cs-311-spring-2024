from pathlib import Path
from sys import argv, exit

from canvasapi import Canvas

course = Canvas("https://canvas.pdx.edu", argv[1]).get_course(84212)

Path("deploy").mkdir(exist_ok=True)

basename = lambda filename: str(filename).split('/')[-1].split('.')[0]


##########
## info ##
##########

info_module = course.get_module(472862)
already_uploaded_info = {item.title for item in info_module.get_module_items()}

for filename in Path("info").glob("*.pdf"):
  success, upload = course.upload(filename, parent_folder_path="info")
  if not success:
    print("failed to upload {}".format(filename))
    exit(-1)

  title = basename(filename)
  if title not in already_uploaded_info:
    info_module.create_module_item({
      "title": title,
      "type": "File",
      "content_id": upload["id"]
    })


################
## apb slides ##
################

info_module = course.get_module(472863)
already_uploaded_info = {item.title for item in info_module.get_module_items()}

for filename in Path("apb-slides").glob("*.pdf"):
  success, upload = course.upload(filename, parent_folder_path="info")
  if not success:
    print("failed to upload {}".format(filename))
    exit(-1)

  title = basename(filename)
  if title not in already_uploaded_info:
    info_module.create_module_item({
      "title": title,
      "type": "File",
      "content_id": upload["id"]
    })


#################
## assignments ##
#################

for filename in Path("hw").glob("*.pdf"):
  success, upload = course.upload(filename, parent_folder_path="hw")
  if not success:
    print("failed to upload {}".format(filename))
    exit(-1)


#######################
## code from lecture ##
#######################

info_module = course.get_module(478584)
already_uploaded_info = {item.title for item in info_module.get_module_items()}

for filename in Path("code").glob("*.py"):
  success, upload = course.upload(filename, parent_folder_path="code")
  if not success:
    print("failed to upload {}".format(filename))
    exit(-1)

  title = basename(filename)
  if title not in already_uploaded_info:
    info_module.create_module_item({
      "title": title,
      "type": "File",
      "content_id": upload["id"]
    })
