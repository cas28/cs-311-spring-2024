from dataclasses import dataclass

Symbol = str

@dataclass
class DFA[State]:
    alphabet: set[Symbol]
    states: set[State] # no overlap with alphabet
    start: State       # member of states
    final: set[State]  # subset (or equal to) states
    transition: dict[tuple[State,Symbol], State]

def accepts[State](dfa: DFA[State], input: str) -> bool:
    current: State = dfa.start
    for symbol in input:
        current = dfa.transition[(current, symbol)]
    return current in dfa.final

def fresh(states: set[str]) -> str:
    name = "dead_end"
    while name in states:
        name += "a"
    return name

def complete(dfa: DFA[str]) -> DFA[str]:
    alphabet = dfa.alphabet
    dead_end = fresh(dfa.states)
    states = dfa.states.union({ dead_end })
    start = dfa.start
    final = dfa.final
    transition = { }
    for symbol in alphabet:
        for state in states:
            if (state, symbol) in dfa.transition:
                transition[(state, symbol)] = dfa.transition[(state, symbol)]
            else:
                transition[(state, symbol)] = dead_end
    return DFA(alphabet, states, start, final, transition)

def cartesian_product[S1, S2](set1: set[S1], set2: set[S2]) -> set[tuple[S1, S2]]:
    product = set()
    for state1 in set1:
        for state2 in set2:
            product.add((state1, state2))
    return product

def union_final[S1, S2](dfa1: DFA[S1], dfa2: DFA[S2]) -> set[tuple[S1, S2]]:
    final = set()
    for state1 in dfa1.states:
        for state2 in dfa2.states:
            if state1 in dfa1.final or state2 in dfa2.final:
                final.add((state1, state2))
    return final

def intersection_final[S1, S2](dfa1: DFA[S1], dfa2: DFA[S2]) -> set[tuple[S1, S2]]:
    final = set()
    for state1 in dfa1.states:
        for state2 in dfa2.states:
            if state1 in dfa1.final and state2 in dfa2.final:
                final.add((state1, state2))
    return final

def union_transition[S1, S2](dfa1: DFA[S1], dfa2: DFA[S2]) -> dict[tuple[tuple[S1,S2],Symbol], tuple[S1,S2]]:
    transition = { }
    for state1 in dfa1.states:
        for state2 in dfa2.states:
            for symbol in dfa1.alphabet.union(dfa2.alphabet):
                transition[((state1, state2), symbol)] = (
                    dfa1.transition[(state1, symbol)],
                    dfa2.transition[(state2, symbol)]
                )
    return transition

def union_dfas(dfa1: DFA[str], dfa2: DFA[str]) -> DFA[tuple[str, str]]:
    alphabet = dfa1.alphabet.union(dfa2.alphabet)

    dfa1_complete = complete(
        DFA(
            alphabet,
            dfa1.states,
            dfa1.start,
            dfa1.final,
            dfa1.transition
        )
    )

    dfa2_complete = complete(
        DFA(
            alphabet,
            dfa2.states,
            dfa2.start,
            dfa2.final,
            dfa2.transition
        )
    )

    states = cartesian_product(dfa1_complete.states, dfa2_complete.states)
    start = (dfa1_complete.start, dfa2_complete.start)
    final = union_final(dfa1_complete, dfa2_complete)
    transition = union_transition(dfa1_complete, dfa2_complete)
    return DFA(alphabet, states, start, final, transition)

example1: DFA[str] = DFA(
    { "a", "b" },
    { "even", "odd" },
    "even",
    { "even" },
    {
        ("even", "a"): "odd",
        ("even", "b"): "even",
        ("odd", "a"): "even",
        ("odd", "b"): "odd",
    }
)

example2: DFA[str] = DFA(
    { "a", "b" },
    { "even", "odd" },
    "even",
    { "even" },
    {
        ("even", "a"): "even",
        ("even", "b"): "odd",
        ("odd", "a"): "odd",
        ("odd", "b"): "even",
    }
)

example_union = union_dfas(example1, example2)

# accepts(union_dfas(dfa1, dfa2), some_string) ==
# accepts(dfa1, some_string) || accepts(dfa2, some_string)
