from dataclasses import dataclass
from itertools import chain, combinations
from typing import Iterable, Union

type DFATransition[Symbol, State] = dict[
    tuple[Symbol, State],
    State
]

type NFATransition[Symbol, State] = dict[
    tuple[Union[Epsilon,Symbol], State],
    frozenset[State]
]

@dataclass(frozen=True)
class Epsilon:
    pass

epsilon = Epsilon()

@dataclass
class NFA[Symbol, State]:
    alphabet: frozenset[Symbol]
    states: frozenset[State] # no overlap with alphabet
    start: State             # member of states
    final: frozenset[State]  # subset (or equal to) states
    transition: NFATransition[Symbol, State]

@dataclass
class DFA[Symbol, State]:
    alphabet: frozenset[Symbol]
    states: frozenset[State] # no overlap with alphabet
    start: State       # member of states
    final: frozenset[State]  # subset (or equal to) states
    transition: DFATransition[Symbol, State]

def epsilon_closure[Symbol, State](
    nfa: NFA[Symbol, State],
    start: State
) -> frozenset[State]:
    seen = set()
    stack = [ start ]
    while stack:
        current = stack.pop()
        seen.add(current)
        next_states = nfa.transition[(epsilon, current)]
        for next_state in next_states:
            if next_state not in seen:
                stack.append(next_state)
    return frozenset(seen)

def powerset[A](iterable: Iterable[A]) -> frozenset[frozenset[A]]:
    s = list(iterable)
    return frozenset(chain.from_iterable(map(frozenset,combinations(s, r)) for r in range(len(s)+1)))

def nfa_to_dfa[Symbol, State](
    nfa: NFA[Symbol, State]
) -> DFA[Symbol, frozenset[State]]:
    alphabet = nfa.alphabet
    dfa_states = powerset(nfa.states)
    start = epsilon_closure(nfa, nfa.start)

    final: frozenset[frozenset[State]] = frozenset(
        dfa_state
        for dfa_state
        in dfa_states
        if len(dfa_state.intersection(nfa.final)) > 0
    )

    transition: DFATransition[Symbol, frozenset[State]] = { }

    for dfa_state in dfa_states:
        for symbol in nfa.alphabet:
            epsilon_closure_of_state = set()
            for nfa_state in dfa_state:
                epsilon_closure_of_state |= epsilon_closure(nfa, nfa_state)
            next_dfa_state = set()
            for nfa_state in epsilon_closure_of_state:
                next_dfa_state |= nfa.transition[(symbol, nfa_state)]
            transition[(symbol, dfa_state)] = frozenset(next_dfa_state)

    return DFA[Symbol, frozenset[State]](
        alphabet,
        dfa_states,
        start,
        final,
        transition,
    )

def nfa_accepts[Symbol, State](
    nfa: NFA[Symbol, State],
    input: list[Symbol]
) -> bool:
    current_states = epsilon_closure(nfa, nfa.start)
    for symbol in input:
        next_states = frozenset()
        for current_state in current_states:
            one_step_states = nfa.transition[(symbol, current_state)]
            for one_step_state in one_step_states:
                next_states |= epsilon_closure(nfa, one_step_state)
        current_states = next_states
    return len(current_states.intersection(nfa.final)) > 0

def dfa_accepts[Symbol, State](
    dfa: DFA[Symbol, State],
    input: list[Symbol]
) -> bool:
    current: State = dfa.start
    for symbol in input:
        current = dfa.transition[(symbol, current)]
    return current in dfa.final

def fresh(states: frozenset[str]) -> str:
    name = "dead_end"
    while name in states:
        name += "a"
    return name

# machine that recognizes (even a's and even b's)
# or starting with at least 3 a's
test1: NFA[str, str] = NFA(
    frozenset({ "a", "b" }),
    frozenset({ "start", "0a", "1a", "2a", "3a", "ee", "eo", "oe", "oo" }),
    "start",
    frozenset({ "3a", "ee" }),
    {
        (epsilon, "start"): frozenset({ "0a", "ee" }),
        (epsilon, "0a"): frozenset(),
        (epsilon, "1a"): frozenset(),
        (epsilon, "2a"): frozenset(),
        (epsilon, "3a"): frozenset(),
        (epsilon, "ee"): frozenset(),
        (epsilon, "eo"): frozenset(),
        (epsilon, "oe"): frozenset(),
        (epsilon, "oo"): frozenset(),
        ("a", "start"): frozenset(),
        ("b", "start"): frozenset(),
        ("a", "0a"): frozenset({ "1a" }),
        ("a", "1a"): frozenset({ "2a" }),
        ("a", "2a"): frozenset({ "3a" }),
        ("a", "3a"): frozenset({ "3a" }),
        ("b", "0a"): frozenset(),
        ("b", "1a"): frozenset(),
        ("b", "2a"): frozenset(),
        ("b", "3a"): frozenset({ "3a" }),
        ("a", "ee"): frozenset({ "oe" }),
        ("a", "eo"): frozenset({ "oo" }),
        ("a", "oe"): frozenset({ "ee" }),
        ("a", "oo"): frozenset({ "eo" }),
        ("b", "ee"): frozenset({ "eo" }),
        ("b", "eo"): frozenset({ "ee" }),
        ("b", "oe"): frozenset({ "oo" }),
        ("b", "oo"): frozenset({ "oe" }),
    }
)

# def complete(dfa: DFA[str]) -> DFA[str]:
#     alphabet = dfa.alphabet
#     dead_end = fresh(dfa.states)
#     states = dfa.states.union({ dead_end })
#     start = dfa.start
#     final = dfa.final
#     transition = { }
#     for symbol in alphabet:
#         for state in states:
#             if (state, symbol) in dfa.transition:
#                 transition[(state, symbol)] = dfa.transition[(state, symbol)]
#             else:
#                 transition[(state, symbol)] = dead_end
#     return DFA(alphabet, states, start, final, transition)

# def cartesian_product[S1, S2](set1: frozenset[S1], set2: frozenset[S2]) -> frozenset[tuple[S1, S2]]:
#     product = frozenset()
#     for state1 in set1:
#         for state2 in set2:
#             product.add((state1, state2))
#     return product

# def union_final[S1, S2](dfa1: DFA[S1], dfa2: DFA[S2]) -> frozenset[tuple[S1, S2]]:
#     final = frozenset()
#     for state1 in dfa1.states:
#         for state2 in dfa2.states:
#             if state1 in dfa1.final or state2 in dfa2.final:
#                 final.add((state1, state2))
#     return final
# 
# def intersection_final[S1, S2](dfa1: DFA[S1], dfa2: DFA[S2]) -> frozenset[tuple[S1, S2]]:
#     final = frozenset()
#     for state1 in dfa1.states:
#         for state2 in dfa2.states:
#             if state1 in dfa1.final and state2 in dfa2.final:
#                 final.add((state1, state2))
#     return final
# 
# def union_transition[S1, S2](dfa1: DFA[S1], dfa2: DFA[S2]) -> dict[tuple[tuple[S1,S2],Symbol], tuple[S1,S2]]:
#     transition = { }
#     for state1 in dfa1.states:
#         for state2 in dfa2.states:
#             for symbol in dfa1.alphabet.union(dfa2.alphabet):
#                 transition[((state1, state2), symbol)] = (
#                     dfa1.transition[(state1, symbol)],
#                     dfa2.transition[(state2, symbol)]
#                 )
#     return transition
# 
# def union_dfas(dfa1: DFA[str], dfa2: DFA[str]) -> DFA[tuple[str, str]]:
#     alphabet = dfa1.alphabet.union(dfa2.alphabet)
# 
#     dfa1_complete = complete(
#         DFA(
#             alphabet,
#             dfa1.states,
#             dfa1.start,
#             dfa1.final,
#             dfa1.transition
#         )
#     )
# 
#     dfa2_complete = complete(
#         DFA(
#             alphabet,
#             dfa2.states,
#             dfa2.start,
#             dfa2.final,
#             dfa2.transition
#         )
#     )
# 
#     states = cartesian_product(dfa1_complete.states, dfa2_complete.states)
#     start = (dfa1_complete.start, dfa2_complete.start)
#     final = union_final(dfa1_complete, dfa2_complete)
#     transition = union_transition(dfa1_complete, dfa2_complete)
#     return DFA(alphabet, states, start, final, transition)
# 
# example1: DFA[str] = DFA(
#     { "a", "b" },
#     { "even", "odd" },
#     "even",
#     { "even" },
#     {
#         ("even", "a"): "odd",
#         ("even", "b"): "even",
#         ("odd", "a"): "even",
#         ("odd", "b"): "odd",
#     }
# )
# 
# example2: DFA[str] = DFA(
#     { "a", "b" },
#     { "even", "odd" },
#     "even",
#     { "even" },
#     {
#         ("even", "a"): "even",
#         ("even", "b"): "odd",
#         ("odd", "a"): "odd",
#         ("odd", "b"): "even",
#     }
# )
# 
# example_union = union_dfas(example1, example2)
# 
# # accepts(union_dfas(dfa1, dfa2), some_string) ==
# # accepts(dfa1, some_string) || accepts(dfa2, some_string)
