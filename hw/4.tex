\documentclass[parskip]{scrartcl}
\usepackage[margin=1in]{geometry}
\usepackage[T1]{fontenc}
\usepackage{fancyvrb}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{enumitem}
\usepackage{tikz}
\usepackage{hyperref}
\usepackage{MnSymbol}
\usepackage{tabularray}

\subject{CS 311 Computational Structures}
\author{Katie Casamento}
\date{Spring 2023}
\publishers{Portland State University}

\title{Assignment 4}
\subtitle{Regular and non-regular languages}

% straight quotes in \mathtt
\DeclareMathSymbol{\mathdblquotechar}{\mathalpha}{letters}{`"}
\newcommand{\mathdblquote}{\mathtt{\mathdblquotechar}}
\begingroup\lccode`~=`"\lowercase{\endgroup
  \let~\mathdblquote
}
\AtBeginDocument{\mathcode`"="8000 }

\begin{document}

\maketitle

Each exercise is worth six points, for a total of 36 points.

If you're working on paper, it's fine to ``scan'' your work with a phone camera. Just \textbf{make sure to set the lighting and contrast so that your writing is clearly readable}.

\section*{Pumping lemma review}

This is just a quick review to clarify what your responses are expected to look like on this assignment. Review the lecture videos for more explanation and examples.

A non-regular language is just defined as any formal language (set of finite strings over a finite alphabet) which is not regular. The pumping lemma is a statement that is true for all regular languages, and therefore false for all non-regular languages. The pumping lemma says:

\begin{tblr}{colspec={X[1.5]|X}}
  For every regular language $L$ over alphabet $\Sigma$ & $\forall (L \in \Sigma^*).\ L$ is regular $\to$ \\
  there is some nonzero \emph{pumping length} $p$, such that & $\exists (p \in \mathbb{N}).\ p > 0\ \wedge$ \\
  for every string in $L$ at least $p$ symbols long & $\forall (w \in L).\ |w| \ge p \to$ \\
  the string can be broken up into a beginning, middle, and end, such that & $\exists (x, y, z \in \Sigma^*).\ w = xyz\ \wedge$ \\
  the middle is nonempty, and & $|y| > 0\ \wedge$ \\
  the beginning and middle are both within the first $p$ characters, and & $|xy| \le p\ \wedge$ \\
  no matter how many times you repeat (``pump'') the middle, you will always get a string in $L$. & $\forall (n \in \mathbb{N}).\ xy^nz \in L$
\end{tblr}

All together:

  $\forall (L \in \Sigma^*).\ L$ is regular $\to \exists (p \in \mathbb{N}).\ p > 0 \wedge \forall (w \in L).\ |w| \ge p \to \exists (x, y, z \in \Sigma^*).\\ w = xyz \wedge |y| > 0 \wedge |xy| \le p \wedge \forall (n \in \mathbb{N}).\ xy^nz \in L$

To prove that a language is non-regular with the pumping lemma:

\begin{enumerate}
  \item Assume that the language \textbf{is} regular, which gives you all of the consequences of the pumping lemma as additional assumptions (since the pumping lemma is true for all regular languages). You don't need to write down this step in this assignment, but it's conceptually an important step.
  \item Define a string $w$, which must be \textbf{in the language} and \textbf{at least $p$ symbols long}. Your definition of $w$ can refer to the pumping length $p$. You must not make \textbf{any} assumptions about $p$ except that it is nonzero, but you can define other numbers in terms of $p$ and then define $w$ in terms of those other numbers. For example, it is not valid to assume $p$ is even, but it is valid to define a new constant $q$ as ``the smallest even number greater than or equal to $p$'' (since it's guaranteed to exist) and then define $w$ in terms of $q$.
  \item Make some mathematical observations about how the \textbf{definition of the language} interacts with the \textbf{assumptions of the pumping lemma}. You must not make \textbf{any} assumptions about $x$, $y$, or $z$, except the four assumptions given to you: $w = xyz$, $|y| > 0$, $|xy| \le p$, and $\forall (n \in \mathbb{N}).\ xy^nz \in L$. For example, if $w = a^pb^p$, the assumption $|xy| \le p$ guarantees that $x$ and $y$ only contain $a$'s, but no assumption guarantees that $z$ only contains $b$'s.
  \item Define a number $n$ and show that $xy^nz \notin L$, justified by your observations in step 3. Your choice of $n$ can be any natural number (non-negative integer including zero), and your definition of $n$ can refer to the pumping length $p$ and string lengths like $|x|$ and $|yz|$.
  \item But by the pumping lemma, we must also have $xy^nz \in L$! Act a little surprised about this and conclude that your initial assumption must have been wrong, and therefore $L$ is non-regular. Write a little square ($\qedsymbol$ or $\blacksquare$, pronounced ``Q.E.D.'') to declare that you've thoroughly checked your proof and you stand behind it. You don't need to write down this step in this assignment, but it's conceptually an important step.
\end{enumerate}

To summarize, each of your pumping lemma proofs for this assignment should have at least a definition of the string $w$, a definition of the number $n$, and an argument that $xy^nz \notin L$. Your argument should generally involve both English and math notation to describe a sequence of logical reasoning. You don't need to stick to any particular writing style or format, just try to be clear about how your justification relates to the problem.

Here's a proof for the language $L = \{a^nb^n\}$, written in a minimal format that's acceptable for this assignment:

\fbox{\parbox{\textwidth}{
  Let $w = a^pb^p$. By the assumption $|xy| \le p$, $y$ must only contain $a$'s, and by the assumption $|y| > 0$ it must contain a nonzero amount of $a$'s. Let $n = 0$. $xyz$ has the same number of $a$'s and $b$'s, so $xy^0z = xz$ must have strictly fewer $a$'s than $b$'s, so $xy^0z \notin L$.
}}

Most of these exercises will require at least a little more careful reasoning, though; keep in mind that $\{a^nb^n\}$ is the simplest non-regular language, so it isn't exactly representative of all non-regular languages.

\section*{Exercises}

For exercises 1-4, prove that each language is non-regular.

\begin{enumerate}[noitemsep]
  \item $L = \{a^mb^nc^m\}$
  \item $L = \{a^mb^n\ |\ 2n < m,\ n > 1\}$
  \item $L = \{a^mb^nc^{m*n}\}$
  \item $L = \{w_1w_2\ |\ w_1, w_2 \in \{a, b\}^*,\ w_2$ is the reverse of $w_1\}$
\end{enumerate}

For exercises 5-6, decide whether each language is regular or non-regular and prove your answer. Assume that the empty string is not a valid binary representation of any number.

\begin{enumerate}[resume,noitemsep]
  \item $L = \{w\ |\ w \in \{0, 1\}^*,\ w$ is the binary representation of a power of 2$\}$
  \item $L = \{0^nw\ |\ w \in \{0, 1\}^*,\ w$ is the binary representation of $n\}$
\end{enumerate}

\end{document}
